package br.mvpcortes.batata.service.processor;


import br.mvpcortes.batata.model.Processing;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;

public class FailProcessor implements Processor {

    @Override
    public String getType() {
        return "i_will_fail";
    }

    @Override
    public Flux<DataBuffer> run(Processing p) {
        return Flux.error(new IllegalStateException("my_lovely_exception_S2"));
    }

    @Override
    public MediaType getMediaType(Processing p) {
        return MediaType.APPLICATION_PDF;
    }
}
