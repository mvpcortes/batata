package br.mvpcortes.batata.service;

import br.mvpcortes.batata.TestApplication;
import br.mvpcortes.batata.dao.jdbc.ProcessingJdbcDAO;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.storage.FileDataBufferStorage;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes={TestApplication.class})
public class ProcessingScheduleTest {

    @Autowired
    ProcessingSchedule processingSchedule;

    @Autowired
    private ProcessingJdbcDAO processingJdbcDAO;

    @Autowired
    private BatataFileStorageProperties batataFileStorageProperties;

    @Autowired
    private FileDataBufferStorage fileDataBufferStorage;

    @TempDir
    public Path tempDirectory;

    @BeforeEach
    public void init(){

        processingJdbcDAO.deleteAll();
        batataFileStorageProperties.setPath(tempDirectory);
    }

    @Test
    public void when_run_schedule_and_there_not_is_processing_then_do_nothing(){
        processingSchedule.runProcessings();
        assertThat(processingJdbcDAO.count()).isEqualTo(0);
    }


    @Test
    public void when_run_schedule_and_there_is_one_processing_then_built_it(){
        Processing processing = new Processing("json_argument", Map.of("1", "1", "2", "2", "3", "3"));

        StepVerifier.create(processingJdbcDAO.save(processing))
                .expectNextCount(1)
                .verifyComplete();


        processingSchedule.runProcessings();

        assertThat(processingJdbcDAO.count()).isEqualTo(1);

        List<Processing> list = new ArrayList<>();

        processingJdbcDAO.findById(processing.getId()).subscribe(list::add);

        Processing p = list.get(0);

        assertThat(p).isEqualTo(processing);

        assertThat(p.getStatus()).isEqualTo(Processing.Status.ENDED);
        assertThat(p.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
        assertThat(p.getStartTime()).isAfter(p.getCreateTime());
        assertThat(p.getEndTime()).isAfter(p.getStartTime());

        //check file content
        StepVerifier.create(fileDataBufferStorage.findDataById(p.getId())
                .map(db->db.toString(Charset.defaultCharset()))
                .collect(Collectors.joining())
        )
                .expectNext("{\"1\":\"1\",\"2\":\"2\",\"3\":\"3\"}")
                .verifyComplete();
    }

    @Nested
    public class CleaningProcessings {

        private Processing processing;
        @BeforeEach
        public void createProcessing(){
            processing = new Processing("json_argument", Map.of("1", "1", "2", "2", "3", "3"));

            StepVerifier.create(processingJdbcDAO.save(processing))
                    .expectNextCount(1)
                    .verifyComplete();

            processingSchedule.runProcessings();
        }

        @Test
        public void when_there_is_processing_ran_and_run_cleaning_then_delete_it() throws InterruptedException {

            assertThat(processingJdbcDAO.count()).isEqualTo(1);

            //
            Thread.sleep(10);//wait for 10 milliseconds to try get the processed processing
            long[] sums = processingSchedule.runCleaning(Duration.ofMillis(1));

            SoftAssertions.assertSoftly(sa -> {
                sa.assertThat(sums[0]).isEqualTo(1);

                sa.assertThat(sums[1]).isEqualTo(1);

                sa.assertThat(processingJdbcDAO.count()).isZero();
            });
        }

        @Test
        public void when_there_is_processing_ran_but_storage_are_deleted_and_run_cleaning_then_delete_it_and_not_count_storage_deletion() throws InterruptedException {

            assertThat(processingJdbcDAO.count()).isEqualTo(1);

            fileDataBufferStorage.deleteById(processing.getId()).block();

            //
            Thread.sleep(10);//wait for 10 milliseconds to try get the processed processing
            long[] sums = processingSchedule.runCleaning(Duration.ofMillis(1));

            SoftAssertions.assertSoftly(sa -> {
                sa.assertThat(sums[0]).isEqualTo(1);

                sa.assertThat(sums[1]).isZero();

                sa.assertThat(processingJdbcDAO.count()).isZero();
            });
        }
    }

    @Test
    public void when_run_schedule_and_there_is_one_processing_but_processor_fail_then_save_failed_processing() {
        Processing newProcessing = new Processing("i_will_fail",
                Map.of("fail_fail", "ff"));

        final Processing savedProcessing = processingJdbcDAO.save(newProcessing).block();

        processingSchedule.runProcessings();

        final Processing processingProcessed = processingJdbcDAO.findById(newProcessing.getId()).block();

        SoftAssertions.assertSoftly(sa -> {
            sa.assertThat(processingProcessed.getCreateTime()).isEqualToIgnoringNanos(savedProcessing.getCreateTime());
            sa.assertThat(processingProcessed.getType()).isEqualTo("i_will_fail");
            sa.assertThat(processingProcessed.getId()).isEqualTo(savedProcessing.getId());
            sa.assertThat(processingProcessed.getError())
                    .containsSubsequence(
                            "java.lang.IllegalStateException: my_lovely_exception_S2",
                            "at br.mvpcortes.batata.dao.jdbc.ProcessingJdbcDAO",
                            "br.mvpcortes.batata.service.ProcessingSchedule.runProcessings",
                            "ProcessingScheduleTest.java"
                    );
            sa.assertThat(processingProcessed.getEndTime()).isAfter(savedProcessing.getCreateTime());
            sa.assertThat(processingProcessed.getStartTime()).isAfter(savedProcessing.getCreateTime());
            sa.assertThat(processingProcessed.getStatus()).isEqualTo(Processing.Status.FAILED);
        });
    }

    @Test
    public void when_run_schedule_and_there_are_three_processing_but_second_processor_fail_then_run_all_them_and_register_second_falled(){
        final Processing processingFirst = new Processing("json_argument", Map.of("name", "Im first"));
        final Processing processingSecond = new Processing("i_will_fail", Map.of("name", "Im second"));
        final Processing processingThird = new Processing("json_argument", Map.of("name", "Im third"));

        List.of(processingFirst, processingSecond, processingThird).forEach(p->processingJdbcDAO.save(p).block());

        processingSchedule.runProcessings();

        final Processing pFirstProcessed  = processingJdbcDAO.findById(processingFirst.getId()).block();
        final Processing pSecondProcessed = processingJdbcDAO.findById(processingSecond.getId()).block();
        final Processing pThirdProcessed  = processingJdbcDAO.findById(processingThird.getId()).block();

        assertThat(pFirstProcessed.getStatus()).isEqualTo(Processing.Status.ENDED);
        assertThat(pSecondProcessed.getStatus()).isEqualTo(Processing.Status.FAILED);
        assertThat(pThirdProcessed.getStatus()).isEqualTo(Processing.Status.ENDED);

        StepVerifier.create(fileDataBufferStorage.findDataById(pFirstProcessed.getId())
                .map(db->db.toString(Charset.defaultCharset()))
                .collect(Collectors.joining())
        )
                .expectNext("{\"name\":\"Im first\"}")
                .verifyComplete();

        StepVerifier.create(fileDataBufferStorage.findDataById(pSecondProcessed.getId())
                .map(db->db.toString(Charset.defaultCharset()))
                .collect(Collectors.joining())
        )
                .expectNext("")
                .verifyComplete();

        StepVerifier.create(fileDataBufferStorage.findDataById(pThirdProcessed.getId())
                .map(db->db.toString(Charset.defaultCharset()))
                .collect(Collectors.joining())
        )
                .expectNext("{\"name\":\"Im third\"}")
                .verifyComplete();
    }

}