package br.mvpcortes.batata.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class EntityNotFoundExceptionTest {

    EntityNotFoundException entityNotFoundException;

    @BeforeEach
    public void init(){
        entityNotFoundException = new EntityNotFoundException("xuxu", String.class);
    }

    @Test
    public void verify_constructor_message(){
        assertThat(entityNotFoundException.getMessage()).isEqualTo("Entity 'java.lang.String' with id 'xuxu' not found");
    }

    @Test
    public void verify_id(){
        assertThat(entityNotFoundException.getId()).isEqualTo("xuxu");
    }

    @Test
    public void verify_clazz(){
        assertThat(entityNotFoundException.getClazz()).isEqualTo(String.class);
    }

}