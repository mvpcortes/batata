package br.mvpcortes.batata.service.processor;


import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.processor.Processor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class ProcessorTest {

    static class MyProcessor implements Processor {

        @Override
        public Flux<DataBuffer> run(Processing p) {
            return Flux.empty();
        }

        @Override
        public MediaType getMediaType(Processing p) {
            return MediaType.TEXT_PLAIN;
        }
    }


    private Processing processing;

    private Processor processor;

    @BeforeEach
    public void init(){
        processor = new MyProcessor();

        processing =  new Processing().setType("my_name_processing")
                .addArgs(Map.of("a", "aa", "b", "bb", "z", "zzzz"));

        processing.setId("pinapple");
    }
    @Test
    public void verify_default_value_for_type_of_processor(){
        assertThat(processor.getType())
                .isEqualTo("br.mvpcortes.batata.service.processor.ProcessorTest$MyProcessor");
    }

    @Test
    public void verify_get_file_name(){
        assertThat(processor.getFileName(processing))
                .isEqualTo("pinapple");
    }

    @Test
    public void verify_get_download_data(){
        Processor.DownloadData dd = processor.getDownloadData(processing);

        assertThat(dd.mediaType())
                .isEqualTo(MediaType.TEXT_PLAIN);
        assertThat(dd.fileName()).isEqualTo("pinapple");
    }

}