package br.mvpcortes.batata.service;


import br.mvpcortes.batata.TestApplication;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.processor.Processor;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes={TestApplication.class, ProcessorSearcherTest.CreateProcessorsConfiguration.class})
class ProcessorSearcherTest {

    @TestConfiguration
    public static class CreateProcessorsConfiguration{

        @Bean
        public Processor processorA(){
            return new Processor() {

                @Override
                public String getType() {
                    return "processorA";
                }

                @Override
                public Flux<DataBuffer> run(Processing p) {
                    return Flux.empty();
                }

                @Override
                public MediaType getMediaType(Processing p) {
                    return MediaType.APPLICATION_JSON;
                }
            };
        }

        @Bean
        public Processor processorB(){
            return new Processor() {
                @Override
                public String getType() {
                    return "processorB";
                }

                @Override
                public Flux<DataBuffer> run(Processing p) {
                    return Flux.empty();
                }

                @Override
                public MediaType getMediaType(Processing p) {
                    return MediaType.APPLICATION_JSON;
                }
            };
        }
    }


    @Autowired
    private ProcessorSearcher processorSearcher;

    @Test
    public void when_processor_A_exists_then_return_it(){
        Processor pA = processorSearcher.searchProcessor(createMockProcessing("processorA"));
        assertThat(pA.getType()).isEqualTo("processorA");
    }

    @Test
    public void when_processor_B_exists_then_return_it(){
        Processor pB = processorSearcher.searchProcessor(createMockProcessing("processorB"));
        assertThat(pB.getType()).isEqualTo("processorB");
    }

    @Test
    public void when_processor_not_exists_then_fail(){
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(()-> processorSearcher.searchProcessor(createMockProcessing("xuxu")))
                .withMessage("Type 'xuxu' of processing '21a7e11cca05efb3178c41ef8f3b3cd3077dccb8c0e5a0a4a9a0f02e54899fc6(CREATED)' does not found.")
                .withNoCause();
    }

    private Processing createMockProcessing(String type) {
        Processing processing = new Processing(type, Map.of("type", type));
        processing.setId(processing.daoHashCode());

        return processing;
    }

}