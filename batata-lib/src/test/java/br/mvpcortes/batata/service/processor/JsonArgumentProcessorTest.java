package br.mvpcortes.batata.service.processor;


import br.mvpcortes.batata.model.Processing;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class JsonArgumentProcessorTest {

    private JsonArgumentProcessor jsonArgumentProcessor;

    private Processing processing;
    @BeforeEach
    public void init(){
        jsonArgumentProcessor = new JsonArgumentProcessor();

        processing = new Processing()
                            .setType("type_for_test_json")
                            .addArg("v", "vv")
                            .addArg("ww", "wwww")
                            .setId("S2_my_beaulty_id_S2");
    }

    @Test
    public void verify_get_download_data(){
        Processor.DownloadData dd = jsonArgumentProcessor
                .getDownloadData(processing);

        assertThat(dd.mediaType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(dd.fileName()).isEqualTo("S2_my_beaulty_id_S2.json");
    }

}