package br.mvpcortes.batata.service.processor;

import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.processor.OutputStreamProcessor;
import br.mvpcortes.batata.service.processor.Processor;
import br.mvpcortes.batata.util.ByteArrayUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class OutputStreamProcessorTest {

    public static class MyProcessor extends OutputStreamProcessor {
        @Override
        protected void run(Processing p, OutputStream outputStream) throws IOException {
            outputStream.write(("data-" + p.getArgument("data")).getBytes());
        }

        @Override
        public MediaType getMediaType(Processing p) {
            return MediaType.TEXT_PLAIN;
        }
    }

    public static class MyFailedProcessor extends OutputStreamProcessor{
        @Override
        protected void run(Processing p, OutputStream outputStream) throws IOException {
            throw new IOException("Fail fail fail");
        }

        @Override
        public MediaType getMediaType(Processing p) {
            return MediaType.TEXT_PLAIN;
        }
    }

    @Test
    public void when_run_outputstream_processor_then_put_values_on_flux_with_buffer_size_1(){
        MyProcessor processor = new MyProcessor();

        processor.setDataBufferSize(1);

        test_my_processor(processor);
    }

    @Test
    public void when_run_outputstream_processor_then_put_values_on_flux_with_buffer_size_5(){
        MyProcessor processor = new MyProcessor();

        processor.setDataBufferSize(5);

        test_my_processor(processor);
    }

    @Test
    public void when_run_OutputStreamProcessor_with_verify_then_ok(){
        final MyProcessor processor = new MyProcessor();
        final Mono<String> mono = createFlux(processor)
                .collectList()
                .map(ByteArrayUtils::merge)
                .map(String::new);

        StepVerifier.create(mono)
                .expectNext("data-1234")
                .verifyComplete();

    }

    @Test
    public void when_run_OutputStreamProcessor_with_buffer_size_10_then_verify_then_ok(){
        MyProcessor processor = new MyProcessor();
        processor.setDataBufferSize(10);

        final Mono<String> flux = createFlux(processor)
                .collectList()
                .map(ByteArrayUtils::merge)
                .map(String::new);


        StepVerifier.create(flux)
                .expectNext("data-1234")
                .verifyComplete();

    }

    @Test
    public void when_run_OutputStreamProcessor_and_fail_then_flux_detect_error(){
        MyFailedProcessor processor = new MyFailedProcessor();
        processor.setDataBufferSize(1);

        final Mono<String> mono = createFlux(processor)
                .collectList()
                .map(ByteArrayUtils::merge)
                .map(String::new);

        StepVerifier.create(mono)
                .expectError(IOException.class)
                .verify();

    }


    private void test_my_processor(MyProcessor processor) {
        List<byte[]> listCollect = new ArrayList<>();

        createFlux(processor)
                .subscribe(listCollect::add)
                .dispose();

        final byte[] bytes = ByteArrayUtils.merge(listCollect);//Bytes.toArray(listCollect);

        String content = new String(bytes);

        assertThat(content).isEqualTo("data-1234");
    }

    private Flux<byte[]> createFlux(Processor processor) {
        return  processor.run(createProcessing())
                .map(DataBuffer::asInputStream)
                .map((InputStream is) -> {
                    try {
                        return is.readAllBytes();
                    } catch (IOException e) {
                        throw new IllegalStateException("fail to read all bytes", e);
                    }
                });
    }

    private Processing createProcessing() {
        return new Processing("teste", Map.of("data", "1234"));
    }

}