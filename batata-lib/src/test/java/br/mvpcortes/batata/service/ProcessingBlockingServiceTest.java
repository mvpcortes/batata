package br.mvpcortes.batata.service;

import br.mvpcortes.batata.dao.jdbc.ProcessingJdbcDAO;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.processor.Processor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ProcessingBlockingServiceTest {

    @TempDir
    public Path tempDirectory;

    @Autowired
    private ProcessingBlockingService processingBlockingService;

    @Autowired
    private ProcessingSchedule processingSchedule;

    @Autowired
    private BatataFileStorageProperties batataFileStorageProperties;

    @Autowired
    private ProcessingJdbcDAO processingJdbcDAO;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init(){
        processingJdbcDAO.deleteAll();
        batataFileStorageProperties.setPath(this.tempDirectory);
    }

    @Test
    public void when_create_a_processing_then_we_can_generate_download_data(){
        Processing processing = processingBlockingService.create("json_argument", Map.of("a", "1"));

        Processor.DownloadData dd = processingBlockingService.findDownloadData(processing.getId());

        assertThat(dd.fileName()).isEqualTo("42000e91ad8d78b2dff8cf1cabf1f36434fa66ca34d7a4c315366385fc3d11e0.json");
        assertThat(dd.mediaType()).isEqualTo(MediaType.APPLICATION_JSON);
    }

    @Test
    public void when_create_a_processing_then_save_it(){
        Processing processing = processingBlockingService.create("json_argument", Map.of("a", "1"));

        assertThat(processing.getId()).isEqualTo("42000e91ad8d78b2dff8cf1cabf1f36434fa66ca34d7a4c315366385fc3d11e0");

        Processing processingSaved = processingBlockingService.findProcessing(processing.getId());

        assertThat(processingSaved.getId()).isEqualTo("42000e91ad8d78b2dff8cf1cabf1f36434fa66ca34d7a4c315366385fc3d11e0");
        assertThat(processingSaved.getStatus()).isEqualTo(Processing.Status.CREATED);
        assertThat(processingSaved.getEndTime()).isNull();
        assertThat(processingSaved.getStartTime()).isNull();
        assertThat(processingSaved.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
        assertThat(processingSaved.getArguments()).isEqualTo(Map.of("a", "1"));
        assertThat(processingSaved.getError()).isNull();
        assertThat(processingSaved.getType()).isEqualTo("json_argument");
    }

    @Test
    public void when_run_processing_then_status_is_changed(){
        Processing processing = processingBlockingService.create("json_argument",
                Map.of("a", "1",
                        "b", "2",
                        "xuxu", "xaxa"));

        processingSchedule.runProcessings();

        Processing processingSaved = processingBlockingService.findProcessing(processing.getId());

        assertThat(processingSaved.getId()).isEqualTo("280ceae3c7d2d93c476029ae47d0b2b6ac040cc879c2b97a00de1c213b079ebe");
        assertThat(processingSaved.getStatus()).isEqualTo(Processing.Status.ENDED);
        assertThat(processingSaved.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
        assertThat(processingSaved.getStartTime()).isAfter(processing.getCreateTime());
        assertThat(processingSaved.getEndTime()).isAfter(processingSaved.getStartTime());
        assertThat(processingSaved.getArguments()).isEqualTo(Map.of("a", "1",
                "b", "2",
                "xuxu", "xaxa"));
        assertThat(processingSaved.getError()).isNull();
        assertThat(processingSaved.getType()).isEqualTo("json_argument");
    }

    @Test
    public void when_run_processing_then_bytes_are_created_on_storage() throws IOException {
        Processing processing = processingBlockingService.create("json_argument",
                Map.of("z", "9",
                        "y", "99",
                        "x", "999",
                        "123", "321"));

        processingSchedule.runProcessings();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        processingBlockingService.findData(processing.getId(), byteArrayOutputStream);

        final Map<String, String> map = objectMapper.readValue(byteArrayOutputStream.toByteArray(), Map.class);
        assertThat(map).containsAllEntriesOf(Map.of("z", "9",
                "y", "99",
                "x", "999",
                "123", "321"));

    }
}