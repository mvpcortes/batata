package br.mvpcortes.batata.util;


import lombok.val;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ResultSetExtendedTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Nested
    public class Quando_tenta_data {
        @Test
        public void quando_existente_entao_ok() {

            LocalDate localDate = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 1", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getLocalDate("UMA_DATA");
            });

            assertThat(localDate).isEqualTo("2020-02-13");
        }


        @Test
        public void quando_optional_existente_entao_ok() {

            Optional<LocalDate> optLocalDate = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 1", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptLocalDate("UMA_DATA");
            });

            assertThat(optLocalDate).contains(LocalDate.of(2020,2,13));
        }


        @Test
        public void quando_inexistente_entao_retorna_vazio() {

            LocalDate localDate = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 3", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getLocalDate("UMA_DATA");
            });

            assertThat(localDate).isNull();
        }


        @Test
        public void quando_optional_inexistente_entao_ok() {

            Optional<LocalDate> optLocalDate = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 3", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptLocalDate("UMA_DATA");
            });

            assertThat(optLocalDate).isEmpty();
        }
    }


    @Nested
    public class Quando_tenta_datetime {
        @Test
        public void quando_existente_entao_ok() {

            LocalDateTime localDateTime = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 3", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getLocalDateTime("UMA_DATA_TEMPO");
            });

            assertThat(localDateTime).isEqualTo("2020-02-15T00:05:00");
        }


        @Test
        public void quando_optional_existente_entao_ok() {

            Optional<LocalDateTime> localDateTime = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 3", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptLocalDateTime("UMA_DATA_TEMPO");
            });

            assertThat(localDateTime).contains(LocalDateTime.of(2020,2,15,0,5,0));
        }


        @Test
        public void quando_inexistente_entao_retorna_vazio() {

            LocalDateTime localDateTime = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 2", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getLocalDateTime("UMA_DATA_TEMPO");
            });

            assertThat(localDateTime).isNull();
        }


        @Test
        public void quando_optional_inexistente_entao_ok() {

            Optional<LocalDateTime> localDateTime = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 2", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptLocalDateTime("UMA_DATA_TEMPO");
            });

            assertThat(localDateTime).isEmpty();
        }
    }


    @Nested
    public class Quando_tenta_int {
        @Test
        public void quando_long_eh_preenchido_entao_retorna_ele() {
            Integer numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 2", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getIntNullable("UM_NUMERO");
            });

            assertThat(numero).isNotNull()
                    .isEqualTo(2);
        }

        @Test
        public void quando_long_eh_nulo_entao_retorna_ele(){
            Integer numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO IS NULL", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getIntNullable("UM_NUMERO");
            });

            assertThat(numero).isNull();
        }

        @Test
        public void quando_opt_eh_preenchido_entao_retorna_ele(){
            Optional<Integer> numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 2", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptInt("UM_NUMERO");
            });

            assertThat(numero).contains(2);
        }

        @Test
        public void quando_numero_eh_nulo_entao_retorna_ele(){
            Optional<Integer> numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO IS NULL", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptInt("UM_NUMERO");
            });

            assertThat(numero).isEmpty();
        }
    }

    @Nested
    public class Quando_tenta_long {
        @Test
        public void quando_long_eh_preenchido_entao_retorna_ele() {
            Long numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 2", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getLongNullable("UM_NUMERO");
            });

            assertThat(numero).isNotNull()
                    .isEqualTo(2L);
        }

        @Test
        public void quando_long_eh_nulo_entao_retorna_ele(){
            Long numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO IS NULL", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getLongNullable("UM_NUMERO");
            });

            assertThat(numero).isNull();
        }

        @Test
        public void quando_opt_eh_preenchido_entao_retorna_ele(){
            Optional<Long> numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 2", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptLong("UM_NUMERO");
            });

            assertThat(numero).contains(2L);
        }

        @Test
        public void quando_numero_eh_nulo_entao_retorna_ele(){
            Optional<Long> numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO IS NULL", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getOptLong("UM_NUMERO");
            });

            assertThat(numero).isEmpty();
        }
    }


    @Nested
    public class Quando_tenta_boolean {
        @Test
        public void quando_boolean_eh_preenchido_true_entao_retorna_ele() {
            Boolean numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 1", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getBoolean("UM_BOOLEANO");
            });

            assertThat(numero).isNotNull().isTrue();
        }

        @Test
        public void quando_boolean_eh_preenchido_false_entao_retorna_ele() {
            Boolean numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 2", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getBooleanNullable("UM_BOOLEANO");
            });

            assertThat(numero).isNotNull()
                    .isFalse();
        }

        @Test
        public void quando_boolean_eh_nulo_entao_retorna_ele(){
            Boolean numero = jdbcTemplate.queryForObject("SELECT * FROM  batata.TESTE_RESULT_SET_EXTENDED WHERE UM_NUMERO = 4", (rs, i) -> {
                val rse = new ResultSetExtended(rs);
                return rse.getBooleanNullable("UM_BOOLEANO");
            });

            assertThat(numero).isNull();
        }
    }

}