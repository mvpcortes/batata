package br.mvpcortes.batata.util;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class ByteArrayUtilsTest {

    @Test
    public void verify_unboxing_array_primitive_to_boxed(){

        final byte[] bytes = {1, 2, 3, 4};

        final Byte[] bytesB = ByteArrayUtils.boxedByteArray(bytes);

        assertThat(bytesB[0]).isEqualTo((byte)1);
        assertThat(bytesB[1]).isEqualTo((byte)2);
        assertThat(bytesB[2]).isEqualTo((byte)3);
        assertThat(bytesB[3]).isEqualTo((byte)4);
        assertThat(bytesB).hasSize(4);
    }

    @Test
    public void verify_boxing_array_primitive_to_boxed(){

        final Byte[] bytesB = {8, 4, 5, 7};

        final byte[] bytes = ByteArrayUtils.unboxedByteArray(bytesB);

        assertThat(bytes[0]).isEqualTo((byte)8);
        assertThat(bytes[1]).isEqualTo((byte)4);
        assertThat(bytes[2]).isEqualTo((byte)5);
        assertThat(bytes[3]).isEqualTo((byte)7);
        assertThat(bytes).hasSize(4);
    }

    @Test
    public void verify_merge_bytearray(){
        final List<byte[]> list = List.of(new byte[]{0, 4, 5}, new byte[]{1}, new byte[]{4, 6, 7, 2});

        byte[] bytesMerged = ByteArrayUtils.merge(list);

        assertThat(bytesMerged).hasSize(8);

        assertThat(bytesMerged[0]).isEqualTo((byte)0);
        assertThat(bytesMerged[1]).isEqualTo((byte)4);
        assertThat(bytesMerged[2]).isEqualTo((byte)5);
        assertThat(bytesMerged[3]).isEqualTo((byte)1);
        assertThat(bytesMerged[4]).isEqualTo((byte)4);
        assertThat(bytesMerged[5]).isEqualTo((byte)6);
        assertThat(bytesMerged[6]).isEqualTo((byte)7);
        assertThat(bytesMerged[7]).isEqualTo((byte)2);
    }

    @Test
    public void verify_merge_bytearray_with_only_one_array(){
        final List<byte[]> list = List.of(new byte[]{8, 47, 10});

        byte[] bytesMerged = ByteArrayUtils.merge(list);

        assertThat(bytesMerged).hasSize(3);

        assertThat(bytesMerged[0]).isEqualTo((byte) 8);
        assertThat(bytesMerged[1]).isEqualTo((byte)47);
        assertThat(bytesMerged[2]).isEqualTo((byte)10);
    }

}