package br.mvpcortes.batata.util;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CreateValueTest {
    @Test
    public void verify_toString(){
        CreateValue createValue = new CreateValue("test", "a", "1", "b", "2");

        assertThat(createValue).hasToString("'test'{a=1, b=2}");
    }
}