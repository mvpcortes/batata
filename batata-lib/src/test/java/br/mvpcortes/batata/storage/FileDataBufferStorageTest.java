package br.mvpcortes.batata.storage;


import br.mvpcortes.batata.service.BatataFileStorageProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class FileDataBufferStorageTest extends BaseDataBufferStorageForTest{

    @TempDir
    public Path tempDirectory;

    private FileDataBufferStorage fileDataBufferStorage;

    //private final DataBufferFactory dataBufferFactory = new DefaultDataBufferFactory();

    @BeforeEach
    public void init(){
        fileDataBufferStorage = new FileDataBufferStorage(
                new BatataFileStorageProperties().setPath(tempDirectory)
        );
    }

    @Test
    public void when_save_a_new_file_then_this_exists_and_have_bytes_saved() {
        assertThat(fileDataBufferStorage.getPathById("1")).doesNotExist();

        StepVerifier.create(fileDataBufferStorage.save("1", Flux.just(createDataBuffer("abc"))))
                .expectNext("1")
                .verifyComplete();


        assertThat(fileDataBufferStorage.getPathById("1"))
                //verify if data is saved
                .exists()
                //verify data
                .hasContent("abc");
    }

    @Test
    public void when_save_a_exist_file_then_override(){
        when_save_a_new_file_then_this_exists_and_have_bytes_saved();

        StepVerifier.create(fileDataBufferStorage.save("1", Flux.just(createDataBuffer("eggs"))))
                .expectNext("1")
                .verifyComplete();

        //verify if data is saved
        assertThat(fileDataBufferStorage.getPathById("1")).exists();

        //verify data
        assertThat(fileDataBufferStorage.getPathById("1")).hasContent("eggs");
    }


    @Test
    public void when_findAllIds_them_list_all_files(){
        Stream.of(1, 5, 8, 11, 33, 103, 434)
                .forEach(i->{
                    try {
                        Files.createFile(this.tempDirectory.resolve(Integer.toString(i)));
                    } catch (IOException e) {
                        throw new IllegalStateException(e);
                    }
                });

        StepVerifier
            .create(fileDataBufferStorage.findAllIds().sort(Comparator.comparing(Long::parseLong)))
                .expectNext("1", "5", "8", "11", "33", "103", "434")
                .verifyComplete();
    }


}