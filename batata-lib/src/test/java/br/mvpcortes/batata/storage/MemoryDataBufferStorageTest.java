package br.mvpcortes.batata.storage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.nio.charset.Charset;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class MemoryDataBufferStorageTest extends BaseDataBufferStorageForTest{

    @Autowired
    private MemoryDataBufferStorage memoryDataBufferStorage;

    @BeforeEach
    public void init(){
        memoryDataBufferStorage = new MemoryDataBufferStorage();
    }

    @Test
    public void when_save_data_buffers_then_can_get_value_from_storage(){
        Mono<String> monoResult = memoryDataBufferStorage.save("1", Flux.just(createDataBuffer("my-data-memory")));

        StepVerifier.create(monoResult)
                .expectNext("1")
                .verifyComplete();

        final byte[] data = memoryDataBufferStorage.findRawData("1");

        String result = new String(data);

        assertThat(result).isEqualTo("my-data-memory");
    }

    @Test
    public void when_return_data_buffer_then_return_saved_data(){
        Mono<String> monoResult = memoryDataBufferStorage.save("2", Flux.just(createDataBuffer("my-data-memory-to-return")));

        StepVerifier.create(monoResult)
                .expectNext("2")
                .verifyComplete();

        StepVerifier.create(
                memoryDataBufferStorage
                        .findDataById("2")
                        .map(db->db.toString(Charset.defaultCharset()))
                        .collect(Collectors.joining())
        )
                .expectNext("my-data-memory-to-return")
                .verifyComplete();
    }

    @Test
    public void when_delete_by_id_a_buffer_then_it_not_exists_in_storage(){
        Mono<String> monoResult = memoryDataBufferStorage.save("444", Flux.just(createDataBuffer("my-data-memory-to-be_deleted")));

        StepVerifier.create(monoResult)
                .expectNext("444")
                .verifyComplete();

        StepVerifier.create(memoryDataBufferStorage.findAllIds())
                .expectNext("444")
                .verifyComplete();

        StepVerifier.create(memoryDataBufferStorage.deleteById("444")).expectNext(true).verifyComplete();

        StepVerifier.create(memoryDataBufferStorage.findAllIds())
                .verifyComplete();

    }

    @Test
    public void when_storage_is_empty_then_not_return_ids_by_findAllIds(){
        StepVerifier.create(memoryDataBufferStorage.findAllIds())
                .verifyComplete();

    }
}