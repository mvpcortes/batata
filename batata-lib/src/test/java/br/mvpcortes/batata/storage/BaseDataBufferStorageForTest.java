package br.mvpcortes.batata.storage;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;

public class BaseDataBufferStorageForTest {

    protected DataBufferFactory dataBufferFactory = new DefaultDataBufferFactory();

    protected DataBuffer createDataBuffer(String data) {
        DataBuffer dataBuffer = dataBufferFactory.allocateBuffer(1024);
        dataBuffer.write(data.getBytes());
        return dataBuffer;
    }
}
