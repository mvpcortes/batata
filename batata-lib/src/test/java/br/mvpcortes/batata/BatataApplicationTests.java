package br.mvpcortes.batata;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes=TestApplication.class)
class BatataApplicationTests {

	@SuppressWarnings("EmptyMethod")
	@Test
	void contextLoads() {
	}

}
