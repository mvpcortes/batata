package br.mvpcortes.batata;

import br.mvpcortes.batata.service.processor.FailProcessor;
import br.mvpcortes.batata.service.processor.JsonArgumentProcessor;
import br.mvpcortes.batata.service.processor.Processor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TestApplication {

    @TestConfiguration
    public static class ScheculeConfiguration{

        @Bean
        public Processor failProcessor(){
            return new FailProcessor();
        }

        @Bean
        public Processor jsonArgumentProcessor(){
            return new JsonArgumentProcessor();
        }
    }

}
