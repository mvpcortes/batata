package br.mvpcortes.batata.dao.jdbc;

import br.mvpcortes.batata.TestApplication;
import br.mvpcortes.batata.model.Processing;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes={TestApplication.class})
class ProcessingJdbcDAOTest {

    @Autowired
    private ProcessingJdbcDAO processingJdbcDAO;

    @AfterEach
    public void delete(){
        processingJdbcDAO.deleteAll();
    }

    @Test
    public void when_find_processing_and_not_exists_then_return_empty_mono(){
        StepVerifier.create(processingJdbcDAO.findById("not_exists"))
                .verifyComplete();
    }

    @Test
    public void when_find_processing_and_exists_then_return_mono(){
        final Mono<Processing> monoProcessing = processingJdbcDAO.save(new Processing("test2",
                Map.of("a", "1", "b", "2", "c", "3")));

        List<Processing> list = new ArrayList<>();

        monoProcessing.subscribe(list::add);
        Processing processing = list.get(0);

        StepVerifier.create(processingJdbcDAO.findById(processing.getId()))
                .expectNextMatches(processingGot->{
                    assertThat(processingGot.getId()).isEqualTo(processing.getId());
                    assertThat(processingGot.getType()).isEqualTo(processing.getType());
                    assertThat(processingGot.getArguments()).isEqualTo(processing.getArguments());
                    assertThat(processingGot.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
                    assertThat(processingGot.getEndTime()).isEqualTo(processing.getEndTime());
                    assertThat(processingGot.getStartTime()).isEqualTo(processing.getStartTime());
                    assertThat(processingGot.getError()).isEqualTo(processing.getError());
                    assertThat(processingGot.getStatus()).isEqualTo(processing.getStatus());
                    return true;
                })
                .verifyComplete();
    }

    @Test
    public void when_create_a_processing_then_can_find_this_on_dao(){
        final Processing processing = new Processing("test",
                Map.of("a", "1", "b", "2", "c", "3"));
        StepVerifier.create(processingJdbcDAO.save(processing))
                .consumeNextWith(processing1 -> {
                    assertThat(processing1.getId()).isEqualTo(processing.daoHashCode());
                    assertThat(processing1.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
                    assertThat(processing1.getArguments()).isEqualTo(processing.getArguments());
                    assertThat(processing1.getType()).isEqualTo(processing.getType());
                })
                .verifyComplete();

        StepVerifier.create(processingJdbcDAO.findById(processing.getId()))
                .consumeNextWith(processingSaved ->{
                    assertThat(processingSaved.getId()).isEqualTo(processing.daoHashCode());
                    assertThat(processingSaved.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
                    assertThat(processingSaved.getArguments()).isEqualTo(processing.getArguments());
                    assertThat(processingSaved.getType()).isEqualTo(processing.getType());
                    assertThat(processingSaved.getStartTime()).isNull();
                    assertThat(processingSaved.getEndTime()).isNull();
                } )
                .verifyComplete();
    }

    @Test
    public void when_save_start_processing_then_save_on_database(){
        final Processing processing = new Processing("test",
                Map.of("a", "1", "b", "2", "c", "3"));

        StepVerifier.create(processingJdbcDAO.save(processing))
                .expectNextCount(1)
                .verifyComplete();

        final LocalDateTime before = LocalDateTime.now();
        StepVerifier.create(processingJdbcDAO.saveStartProcessing(processing.getId()))
                .expectNextMatches(ldt->ldt.isAfter(before) && ldt.isBefore(LocalDateTime.now()))
                .verifyComplete();

        StepVerifier.create(processingJdbcDAO.findById(processing.getId()))
                .expectNextMatches(processing1 -> {
                    assertThat(processing1.getId()).isEqualTo(processing.daoHashCode());
                    assertThat(processing1.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
                    assertThat(processing1.getArguments()).isEqualTo(processing.getArguments());
                    assertThat(processing1.getType()).isEqualTo(processing.getType());
                    assertThat(processing1.getStartTime())
                            .isNotNull()
                            .isAfter(processing1.getCreateTime());
                    assertThat(processing1.getEndTime()).isNull();
                    return true;
                })
                .verifyComplete();
    }

    @Test
    public void when_save_end_processing_then_save_on_database(){
        final Processing processing = new Processing("test",
                Map.of("a", "1", "b", "2", "c", "3"));

        StepVerifier.create(processingJdbcDAO.save(processing))
                .expectNextCount(1)
                .verifyComplete();

        final LocalDateTime before = LocalDateTime.now();
        StepVerifier.create(processingJdbcDAO.saveStartProcessing(processing.getId()))
                .expectNextMatches(ldt->ldt.isAfter(before) && ldt.isBefore(LocalDateTime.now()))
                .verifyComplete();

        StepVerifier.create(processingJdbcDAO.saveEndProcessing(processing.getId()))
                .expectNextMatches(ldt->ldt.isAfter(before) && ldt.isBefore(LocalDateTime.now()))
                .verifyComplete();

        StepVerifier.create(processingJdbcDAO.findById(processing.getId()))
                .expectNextMatches(processing1 -> {
                    assertThat(processing1.getId()).isEqualTo(processing.daoHashCode());
                    assertThat(processing1.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
                    assertThat(processing1.getArguments()).isEqualTo(processing.getArguments());
                    assertThat(processing1.getType()).isEqualTo(processing.getType());
                    assertThat(processing1.getStartTime())
                            .isNotNull()
                            .isAfter(processing1.getCreateTime());
                    assertThat(processing1.getEndTime())
                            .isNotNull()
                            .isAfter(processing1.getCreateTime());
                    assertThat(processing1.getError()).isNull();
                    assertThat(processing1.getStatus()).isEqualTo(Processing.Status.ENDED);
                    return true;
                })
                .verifyComplete();
    }

    @Test
    public void when_save_error_processing_then_save_on_database(){
        final Processing processing = new Processing("test",
                Map.of("a", "1", "b", "2", "c", "3"));

        StepVerifier.create(processingJdbcDAO.save(processing))
                .expectNextCount(1)
                .verifyComplete();

        final LocalDateTime before = LocalDateTime.now();
        StepVerifier.create(processingJdbcDAO.saveStartProcessing(processing.getId()))
                .expectNextMatches(ldt->ldt.isAfter(before) && ldt.isBefore(LocalDateTime.now()))
                .verifyComplete();

        StepVerifier.create(processingJdbcDAO.saveErrorProcessing(processing.getId(), new IllegalStateException("xuxu"))
                .map(Processing::getEndTime))
                .expectNextMatches(ldt->ldt.isAfter(before) && ldt.isBefore(LocalDateTime.now()))
                .verifyComplete();

        StepVerifier.create(processingJdbcDAO.findById(processing.getId()))
                .expectNextMatches(processing1 -> {
                    assertThat(processing1.getId()).isEqualTo(processing.daoHashCode());
                    assertThat(processing1.getCreateTime()).isEqualToIgnoringNanos(processing.getCreateTime());
                    assertThat(processing1.getArguments()).isEqualTo(processing.getArguments());
                    assertThat(processing1.getType()).isEqualTo(processing.getType());
                    assertThat(processing1.getStartTime()).isNotNull();
                    assertThat(processing1.getStartTime()).isAfter(processing1.getCreateTime());
                    assertThat(processing1.getEndTime()).isNotNull();
                    assertThat(processing1.getEndTime()).isAfter(processing1.getCreateTime());
                    assertThat(processing1.getStatus()).isEqualTo(Processing.Status.FAILED);
                    return true;
                })
                .verifyComplete();
    }

    @Test
    public void when_exists_4_created_processings_then_return_them_on_findNewProcessings() {
        final Processing pA = new Processing("A", Map.of("1", "1"));
        final Processing pB = new Processing("B", Map.of("2", "2"));
        final Processing pC = new Processing("C", Map.of("3", "3"));
        final Processing pD = new Processing("D", Map.of("4", "4"));

        Flux.just(pA, pB, pC, pD)
                .flatMap(p->processingJdbcDAO.save(p))
                .subscribe();

        StepVerifier.create(this.processingJdbcDAO.findNewProcessings())
                .expectNext(pD, pC, pB, pA)
                .verifyComplete();
    }

    @Test
    public void when_exists_4_created_processings_but_a_was_started_then_return_them_on_findNewProcessings() {
        final Processing pA = new Processing("A", Map.of("1", "1"));
        final Processing pB = new Processing("B", Map.of("2", "2"));
        final Processing pC = new Processing("C", Map.of("3", "3"));
        final Processing pD = new Processing("D", Map.of("4", "4"));

        Flux.just(pA, pB, pC, pD)
                .flatMap(p->processingJdbcDAO.save(p))
                .subscribe();

        Flux.just(pC)
                .flatMap(p->processingJdbcDAO.saveStartProcessing(p.getId()))
                .subscribe();

        StepVerifier.create(this.processingJdbcDAO.findNewProcessings())
                .expectNext(pD, pB, pA)
                .verifyComplete();
    }

    @Test
    public void when_not_exists_created_processings_then_return_empty_on_findNewProcessings() {
        StepVerifier.create(this.processingJdbcDAO.findNewProcessings())
                .verifyComplete();
    }

    @Test
    public void when_verify_processing_exists_then_return_true(){
        val processing = new Processing("XUXUX", Map.of("444", "5555"));
        this.processingJdbcDAO.save(processing).subscribe();

        StepVerifier.create(this.processingJdbcDAO.exists(processing.getId()))
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    public void when_verify_processing_not_exists_then_return_false(){
        StepVerifier.create(this.processingJdbcDAO.exists("little xuxu"))
                .expectNext(false)
                .verifyComplete();
    }


    @Test
    public void when_verify_processings_not_batch_exists_then_return_false_for_all(){
        StepVerifier.create(this.processingJdbcDAO.batchExists(LongStream.range(0L, 100L).mapToObj(Long::toString).collect(Collectors.toList())))
                .verifyComplete();
    }

    @Test
    public void when_verify_processings_batch_exists_then_return_true_for_all(){
        final List<String> listIds = LongStream.range(0L, 100L)
                .mapToObj(Long::toString)
                .map(x->{
                    val p = new Processing(x, Map.of("key"+x, "value"+x));
                    processingJdbcDAO.save(p).subscribe();
                    return p.getId();
                })
                .sorted()
                .collect(Collectors.toList());

        StepVerifier.create(this.processingJdbcDAO.batchExists(listIds)
                .sort()
        )
                .expectNext(listIds.toArray(String[]::new))
                .verifyComplete();
    }

    @Test
    public void when_verify_processings_batch_exists_but_using_subset_then_return_subset(){
        final List<String> listIds = LongStream.range(0L, 100L)
                .mapToObj(Long::toString)
                .map(x->{
                    val p = new Processing(x, Map.of("key"+x, "value"+x));
                    processingJdbcDAO.save(p).subscribe();
                    return p.getId();
                })
                .sorted()
                .filter(x->x.hashCode() % 2 == 1)
                .collect(Collectors.toList());

        StepVerifier.create(this.processingJdbcDAO.batchExists(listIds)
                .sort()
        )
                .expectNext(listIds.toArray(String[]::new))
                .verifyComplete();
    }

    @Test
    public void when_verify_processings_batch_exists_but_using_hiperset_then_return_only_subset(){
        final List<String> listIds = LongStream.range(0L, 100L)
                .mapToObj(Long::toString)
                .map(x -> {
                    val p = new Processing(x, Map.of("key" + x, "value" + x));
                    processingJdbcDAO.save(p).subscribe();
                    return p.getId();
                })
                .sorted()
                .filter(x -> x.hashCode() % 2 == 1)
                .toList();

        val listNew = Stream.concat(listIds.stream(), Stream.of("a", "b", "c"))
                .sorted()
                .collect(Collectors.toList());

        StepVerifier.create(this.processingJdbcDAO.batchExists(listNew)
                .sort()
        )
                .expectNext(listIds.toArray(String[]::new))
                .verifyComplete();
    }
}