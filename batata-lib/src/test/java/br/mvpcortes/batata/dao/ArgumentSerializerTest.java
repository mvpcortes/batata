package br.mvpcortes.batata.dao;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

class ArgumentSerializerTest {

    private ArgumentSerializer argumentSerializer;

    @BeforeEach
    public void init(){
        argumentSerializer = new ArgumentSerializer();
    }

    @Test
    public void when_serialize_a_map_then_it_generate_string(){
        String str = argumentSerializer.serialize(Map.of("election", "2020", "candidate", "orange"));

        assertThat(str).isEqualTo("{\"candidate\":\"orange\",\"election\":\"2020\"}");
    }

    @Test
    public void when_serialize_a_map_and_fail_then_generate_exception() throws JsonProcessingException {
        ObjectMapper objectMapper = mock(ObjectMapper.class);
        doThrow(new JsonMappingException(null, "a")).when(objectMapper).writeValueAsString(Mockito.any(Map.class));

        argumentSerializer = new ArgumentSerializer(objectMapper);

        Assertions.assertThatIllegalStateException()
                .isThrownBy(()->argumentSerializer.serialize(Map.of("election", "2020", "candidate", "orange")))
                .withCauseExactlyInstanceOf(JsonMappingException.class)
                .withMessage("Cannot write map to json: a");

    }


    @Test
    public void when_deserialize_a_String_then_it_generate_Map(){
        Map<String, String> map = argumentSerializer.deserialize("{\"cake\":\"yellow\",\"field\":\"long\"}");

        assertThat(map).containsAllEntriesOf(Map.of("cake", "yellow", "field", "long"));
    }

    @Test
    public void when_deserialize_a_empty_String_then_it_generate_empty_Map(){
        Map<String, String> map = argumentSerializer.deserialize("   ");

        assertThat(map).isEmpty();
    }

    @Test
    public void when_deserialize_a_invalid_string_then_fail(){
        Assertions.assertThatIllegalStateException()
                .isThrownBy(()-> argumentSerializer.deserialize("{888\"cake\":\"yellow\",\"field\":\"long\"}")
                )
                .withMessage("Cannot read arguments from json: Unexpected character ('8' (code 56)): was expecting double-quote to start field name\n" +
                        " at [Source: (String)\"{888\"cake\":\"yellow\",\"field\":\"long\"}\"; line: 1, column: 3]")
                .withCauseExactlyInstanceOf(JsonParseException.class);

    }
}