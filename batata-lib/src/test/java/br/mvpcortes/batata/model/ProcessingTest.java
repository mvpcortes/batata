package br.mvpcortes.batata.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ProcessingTest {

    private Processing processing;

    @BeforeEach
    public void init(){
        processing = new Processing("teste",
                Map.of("a", "1", "b", "2", "c", "3"));
    }

    @Test
    public void when_processing_is_new_then_status_is_created(){
        assertThat(processing.getStatus()).isEqualTo(Processing.Status.CREATED);
    }

    @Test
    public void when_processing_is_started_then_status_is_processing(){
        processing.setStartTime(LocalDateTime.now());
        assertThat(processing.getStatus()).isEqualTo(Processing.Status.PROCESSING);
    }

    @Test
    public void when_processing_is_ended_then_status_is_ended(){
        when_processing_is_started_then_status_is_processing();
        processing.setEndTime(LocalDateTime.now());
        assertThat(processing.getStatus()).isEqualTo(Processing.Status.ENDED);
    }

    @Test
    public void when_processing_is_failed_then_status_is_failed(){
        when_processing_is_ended_then_status_is_ended();
        processing.setError("XuxuXaxa");
        assertThat(processing.getStatus()).isEqualTo(Processing.Status.FAILED);
    }

    @Test
    public void verify_toString(){
        processing.setId(processing.daoHashCode());
        assertThat(processing).hasToString("19673c09eda1d8c7a7b7ddc947a1a276c1ca3817b5f9184072083e7bb0abb061(CREATED)");
    }

    @Test
    public void verify_addArg(){
        processing.addArg("000", "000");
        assertThat(processing.getArguments()).containsAllEntriesOf(
                Map.of("a", "1", "b", "2", "000", "000"));
    }

    @Test
    public void verify_getIntArgument(){
        processing.addArg("xuxu", "3");
        assertThat(processing.getIntArgument("xuxu")).isEqualTo(3);
    }

    @Test
    public void verify_cannot_getIntArgument_with_null_argument(){
        assertThatExceptionOfType(NullPointerException.class)
                .isThrownBy(()->processing.getIntArgument(null));
    }

    @Test
    public void verify_getIntArgument_but_not_exists_then_return_null(){
        assertThat(processing.getIntArgument("not_exists")).isEqualTo(null);
    }

    @Test
    public void verify_addArgs(){
        processing.addArgs(Map.of("000", "000", "111", "111"));
        assertThat(processing.getArguments()).containsAllEntriesOf(
                Map.of("a", "1", "b", "2", "000", "000", "111", "111"));
    }

    @Test
    public void verify_equals_processings_same() {

        assertThat(processing)
                .isEqualTo(processing)
                //noinspection BogusAssertion
                .isSameAs(processing);
    }

    @Test
    public void verify_equals_processings_with_different_object(){
        Processing outro = new Processing("teste", Map.of("a", "1", "b", "2", "c", "3"));
        assertThat(processing)
                .isEqualTo(outro);
        assertThat(processing).isNotSameAs(outro);
    }

    @Test
    public void verify_equals_when_type_is_different(){
        assertThat(processing).isNotEqualTo(new Processing("teste2", Map.of("a", "1", "b", "2")));
    }

    @Test
    public void verify_equals_when_arguments_is_different(){
        assertThat(processing).isNotEqualTo(new Processing("teste", Map.of("b", "2")));
    }

    @Test
    public void verify_hashCode_isEqualTo_daoHashCode(){
        assertThat(processing).hasSameHashCodeAs(processing.daoHashCode());
    }
}