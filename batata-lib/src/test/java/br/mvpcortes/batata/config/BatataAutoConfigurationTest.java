package br.mvpcortes.batata.config;


import org.junit.jupiter.api.Test;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import static org.assertj.core.api.Assertions.assertThat;

class BatataAutoConfigurationTest {

    @Test
    public void when_set_properties_immediate_then_return_this_type(){
        BatataAutoConfiguration configuration = new BatataAutoConfiguration();

        final Scheduler scheduler = configuration.batataScheduler(createProperties("IMMEDIATE"));

        assertThat(scheduler).isSameAs(Schedulers.immediate());
    }

    @Test
    public void when_set_properties_BOUNDED_ELASTIC_then_return_this_type(){
        BatataAutoConfiguration configuration = new BatataAutoConfiguration();

        final Scheduler scheduler = configuration.batataScheduler(createProperties("BOUNDED_ELASTIC"));

        assertThat(scheduler).isSameAs(Schedulers.boundedElastic());
    }

    @Test
    public void when_set_properties_and_not_found_then_return_IMMEDIATE(){
        BatataAutoConfiguration configuration = new BatataAutoConfiguration();

        final Scheduler scheduler = configuration.batataScheduler(createProperties("COCONUT"));

        assertThat(scheduler).isSameAs(Schedulers.immediate());
    }

    BatataSchedulerProperties createProperties(String type){
        return new BatataSchedulerProperties()
                .setType(type);
    }

}