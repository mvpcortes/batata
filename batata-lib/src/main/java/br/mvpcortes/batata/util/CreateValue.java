package br.mvpcortes.batata.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateValue {

    private String type;
    private Map<String, String> arguments;

    public CreateValue(String type, String... mapArguments){
        this.type = type;
        this.arguments = new HashMap<>();

        for(int i = 0; i < mapArguments.length-1; i+=2){
            arguments.put(mapArguments[i], mapArguments[i+1]);
        }
    }

    @Override
    public String toString() {
        return String.format("'%s'%s", type, arguments);
    }
}
