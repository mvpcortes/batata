package br.mvpcortes.batata.util;

import lombok.AllArgsConstructor;
import lombok.experimental.Delegate;
import lombok.val;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@AllArgsConstructor
public class ResultSetExtended implements ResultSet {

    @Delegate(types = ResultSet.class)
    private final ResultSet resultSet;

    public Optional<Long> getOptLong(String label) throws SQLException {
        val valor = getLong(label);
        return resultSet.wasNull() ? Optional.empty():Optional.of(valor);
    }


    public LocalDate getLocalDate(String label) throws SQLException{
        return getOptLocalDate(label).orElse(null);
    }

    public LocalDateTime getLocalDateTime(String label) throws SQLException{
        return getOptLocalDateTime(label).orElse(null);
    }

    public Long getLongNullable(String label) throws SQLException {
        val valor = resultSet.getLong(label);
        return resultSet.wasNull()?null:valor;
    }

    public Optional<LocalDate> getOptLocalDate(String label) throws SQLException{
        return Optional.ofNullable(resultSet.getDate(label))
                .map(Date::toLocalDate);
    }

    public Optional<LocalDateTime> getOptLocalDateTime(String label) throws SQLException{
        return Optional.ofNullable(resultSet.getTimestamp(label))
                .map(ddd-> ddd.toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime());
    }

    public Integer getIntNullable(String label) throws SQLException {
        val valor = resultSet.getInt(label);
        return resultSet.wasNull()?null:valor;
    }

    public Boolean getBooleanNullable(String label) throws SQLException {
        val valor = resultSet.getBoolean(label);
        return resultSet.wasNull()?null:valor;
    }

    public Optional<Integer> getOptInt(String label) throws SQLException {
        val valor = getInt(label);
        return resultSet.wasNull() ? Optional.empty():Optional.of(valor);
    }
}
