package br.mvpcortes.batata.util;

import java.util.List;

public interface ByteArrayUtils {

    static Byte[] boxedByteArray(byte[] barray){
       final Byte[] bbb = new Byte[barray.length];
        for(int i = 0; i < barray.length; i++){
            bbb[i] = barray[i];
        }
        return bbb;
    }

    static byte[] unboxedByteArray(Byte[] barray){
        final byte[] bbb = new byte[barray.length];
        for(int i = 0; i < barray.length; i++){
            bbb[i] = barray[i];
        }
        return bbb;
    }

    static byte[] merge(List<byte[]> list){
        int size = list.stream().mapToInt(bbb -> bbb.length).sum();
        byte[] bytes = new byte[size];
        int offSet = 0;
        for (byte[] byteList : list) {
            System.arraycopy(byteList, 0, bytes, offSet, byteList.length);
            offSet += byteList.length;
        }
        return bytes;
    }
}
