package br.mvpcortes.batata.service.processor;

import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.FluxOutputStream;
import br.mvpcortes.batata.service.processor.Processor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.io.OutputStream;

public abstract class OutputStreamProcessor implements Processor {

    private final DataBufferFactory dataBufferFactory;

    @Getter
    @Setter
    private int dataBufferSize = 1024;

    public OutputStreamProcessor(){
        dataBufferFactory = new DefaultDataBufferFactory();
    }

    public Flux<DataBuffer> run(Processing p){
        return Flux.create(sink-> {
            try (OutputStream outputStream = new FluxOutputStream(sink, dataBufferFactory, dataBufferSize)){
                try {
                    run(p, outputStream);
                    outputStream.flush();
                } catch (IOException e) {
                    sink.error(e);
                }
            }catch(Exception e){
                sink.error(e);
            }
        });
    }


    protected abstract void run(Processing p, OutputStream outputStream) throws IOException;
}
