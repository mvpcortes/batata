package br.mvpcortes.batata.service.processor;

import br.mvpcortes.batata.model.Processing;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;

import java.util.stream.Collectors;

@Slf4j
public class JsonArgumentProcessor implements Processor {

//    @Getter
//    @Setter
    private final DataBufferFactory dataBufferFactory;

    public JsonArgumentProcessor() {
        this.dataBufferFactory = new DefaultDataBufferFactory();
    }

    @Override
    public String getType() {
        return "json_argument";
    }

    @Override
    public Flux<DataBuffer> run(Processing p) {
        return Flux.fromStream(p.getArguments().entrySet().stream())
                .map(entry-> String.format("\"%s\":\"%s\"", entry.getKey(), entry.getValue()))
                .sort()
                .collect(Collectors.joining(",", "{", "}"))
                .map(String::getBytes)
                .map(bytes->dataBufferFactory.allocateBuffer().write(bytes))
                .flux();
    }

    @Override
    public MediaType getMediaType(Processing p) {
        return MediaType.APPLICATION_JSON;
    }

    @Override
    public String getFileName(Processing p) {
        return p.getId() + ".json";
    }
}
