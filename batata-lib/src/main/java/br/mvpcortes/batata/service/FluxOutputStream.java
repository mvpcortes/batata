package br.mvpcortes.batata.service;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import reactor.core.publisher.FluxSink;

import java.io.IOException;
import java.io.OutputStream;

public class FluxOutputStream extends OutputStream {

    private final FluxSink<DataBuffer> fluxSink;

    private final int dataBufferSize;

    private final DataBufferFactory dataBufferFactory;

    private DataBuffer dataBuffer;

    private int qtd;

    public FluxOutputStream(FluxSink<DataBuffer> fluxSink, DataBufferFactory dataBufferFactory, int dataBufferSize) {
        this.fluxSink = fluxSink;
        this.dataBufferFactory = dataBufferFactory;
        this.dataBufferSize = dataBufferSize;

        this.dataBuffer = dataBufferFactory.allocateBuffer(dataBufferSize);
        this.qtd = 0;

    }
    @Override
    public void write(int b) {
        dataBuffer.write((byte) b);
        qtd+=1;
        if(qtd>=dataBufferSize){
            fluxSink.next(dataBuffer);
            createNewDataBuffer();
        }
    }

    @Override
    public void flush() throws IOException {
        super.flush();
        if(!fluxSink.isCancelled()) {
            if(qtd >0){
                fluxSink.next(dataBuffer);
            }
        }
        createNewDataBuffer();
    }

    @Override
    public void close() throws IOException {
        flush();
        if(!fluxSink.isCancelled()) {
            fluxSink.complete();
        }
        super.close();
    }

    protected void createNewDataBuffer(){
        dataBuffer = dataBufferFactory.allocateBuffer(dataBufferSize);
        qtd=0;
    }
}
