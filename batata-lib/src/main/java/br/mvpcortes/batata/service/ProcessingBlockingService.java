package br.mvpcortes.batata.service;

import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.processor.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * This proxy for using Batata with blocking applications (ex. Spring WEB/MVC)
 */
@Service
public class ProcessingBlockingService {

    @Autowired
    private ProcessingService processingService;

    public Processing create(String type, Map<String, String> arguments){
        return processingService.create(type, arguments).block();
    }

    public Processing findProcessing(String id){
        return processingService.findProcessing(id)
                .block();
    }

    public Processor.DownloadData findDownloadData(String id) {
        return processingService.findDownloadData(id).block();
    }

    public void findData(String id, OutputStream outputStream) {
        processingService.findData(id)
                .doOnNext(db-> {
                    try {
                        StreamUtils.copy(db.asInputStream(), outputStream);
                    } catch (IOException e) {
                        throw new IllegalStateException("Cannot read InputStream: " + e.getMessage(), e);
                    }
                })
                .blockLast();
    }
}
