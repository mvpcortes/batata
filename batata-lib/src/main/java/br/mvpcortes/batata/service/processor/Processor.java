package br.mvpcortes.batata.service.processor;

import br.mvpcortes.batata.model.Processing;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;

public interface Processor {

    record DownloadData(MediaType mediaType,String fileName){}

    default String getType(){return this.getClass().getName();}

    Flux<DataBuffer> run(Processing p);

    MediaType getMediaType(Processing p);

    default String getFileName(Processing p){
        return p.getId();
    }

    default DownloadData getDownloadData(Processing p){
        return new DownloadData(getMediaType(p), getFileName(p));
    }
    
}
