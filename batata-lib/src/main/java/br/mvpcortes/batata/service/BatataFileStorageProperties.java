package br.mvpcortes.batata.service;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
@ConfigurationProperties("batata.storage.file")
@Getter
@Setter
@Accessors(chain = true)
@Slf4j
public class BatataFileStorageProperties {

    private Path path;

}
