package br.mvpcortes.batata.service;

import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.processor.Processor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toMap;

@Service
public class ProcessorSearcher {

    private final Map<String, Processor> mapProcessors;

    public ProcessorSearcher(ApplicationContext applicationContext){
        this.mapProcessors = applicationContext.getBeansOfType(Processor.class)
                .values()
                .stream()
                .collect(toMap(Processor::getType, (p)->p));
    }


    public Processor searchProcessor(Processing p) {
        return findProcessor(p.getType())
                .orElseThrow(() -> new IllegalArgumentException("Type '" + p.getType() + "' of processing '" + p + "' does not found."));
    }

    public Optional<Processor> findProcessor(String type){
        return Optional.ofNullable(mapProcessors.get(type));
    }

    public void assertProcessorExists(String type) {
        if(!mapProcessors.containsKey(type)){
            throw new IllegalArgumentException("Processor for type '" + type + "' not exists");
        }
    }
}
