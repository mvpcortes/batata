package br.mvpcortes.batata.service;

import br.mvpcortes.batata.config.BatataSchedulerProperties;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.storage.DataBufferStorage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.time.LocalDateTime;

@Service
@Slf4j
public class ProcessingSchedule {

    @Autowired
    private DataBufferStorage dataBufferStorage;

    @Autowired
    @Qualifier("batataScheduler")
    private Scheduler scheduler;

    @Autowired
    private ProcessorSearcher processorSearcher;

    @Autowired
    private ProcessingService processingService;

    @Autowired
    private BatataSchedulerProperties batataSchedulerProperties;

    @Scheduled(fixedDelayString = "${batata.scheduler.run-delay:300000}")
    public void runScheduledProcessings(){
        if(batataSchedulerProperties.isEnabled()) {
            runProcessings();
        }else{
            log.warn("Scheduler disabled. Not run.");
        }
    }

    @Scheduled(fixedDelayString = "${batata.scheduler.clean-delay:1800000}")
    public void runScheduledCleaning() {
        if(batataSchedulerProperties.isEnabled()) {
            runCleaning(batataSchedulerProperties.getCleanAfter());
        }else{
            log.warn("Scheduler disabled. Not cleaning.");
        }
    }


    public long[] runCleaning(Duration threadshould) {
        return processingService.findProcessingsIdsCreatedBefore(LocalDateTime.now().minus(threadshould))
                .concatWith(processingService.findStoredProcessingWithoutDatabaseInfo())
                .flatMap(processingService::deleteById)
                .map(tuple->tuple.mapT1(x->x?1L:0L).mapT2(x->x?1L:0L))
                .reduce(new long[]{0L, 0L}, ProcessingSchedule::reduceTuplesForCleaning)
                .doOnSuccess((sumValues)->log.info("Finish delete old processings. On Database={}, on storage={}", sumValues[0], sumValues[1]))
                .doOnCancel(()->log.warn("The deleting old processings was cancelled."))
                .onErrorContinue((e, x)-> log.error("Fail to delete old processings: " + e.getMessage(), e))
                .block();
    }


    protected static long[] reduceTuplesForCleaning(long[] sumValues, Tuple2<Long, Long> tuple) {
//we using it to count deleted processings from storage and DAO
        sumValues[0] += tuple.getT1();
        sumValues[1] += tuple.getT2();
        return sumValues;
    }

    /**
     * Main cycle for run Processing
     */
    public void runProcessings() {
        processingService.findNewProcessings()
                .subscribeOn(scheduler)
                .flatMap(p -> processingService.saveStartProcessing(p.getId()).map(p::setStartTime))
                .flatMap(this::runAndSaveProcessor)
                .doOnComplete(()->log.info("Finish run processings"))
                .doOnCancel(()->log.warn("The flux was cancelled."))
                .onErrorContinue((e, x)-> log.error("Fail to process a processing: " + e.getMessage(), e))
                .blockLast();
    }

    private Mono<Processing> runAndSaveProcessor(Processing p) {
        return dataBufferStorage
                .save(p.getId(), runProcessor(p))
                .flatMap(id-> processingService.saveEndProcessing(p.getId()).map(p::setEndTime))
                .doOnNext(pp->log.info("Processsing '{}' was processed", p.getId()))
                .onErrorResume(e-> processingService.saveErrorProcessing(p.getId(), e));
    }


    private Flux<DataBuffer> runProcessor(Processing processing){
        return processorSearcher.searchProcessor(processing)
                .run(processing);
    }

}
