package br.mvpcortes.batata.service;

import lombok.Getter;

import java.util.Optional;

public class EntityNotFoundException extends RuntimeException{

    @Getter
    private final Object id;
    @Getter
    private final Class<?> clazz;

    public EntityNotFoundException(Object id, Class<?> clazz){
        super(String.format("Entity '%s' with id '%s' not found", Optional.ofNullable(clazz).map(Class::getName).orElse("-"), Optional.ofNullable(id).orElse("n/a")));
        this.clazz = clazz;
        this.id = id;
    }
}
