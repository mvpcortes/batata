package br.mvpcortes.batata.service;

import br.mvpcortes.batata.dao.ProcessingDAO;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.processor.Processor;
import br.mvpcortes.batata.storage.DataBufferStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.LocalDateTime;
import java.util.Map;

@Service
public class ProcessingService {

    @SuppressWarnings("FieldCanBeLocal")
    private final int MAX_IDS_BATCH_EXISTS = 500;

    @Autowired
    private ProcessingDAO processingDAO;

    @Autowired
    private DataBufferStorage dataBufferStorage;

    @Autowired
    private ProcessorSearcher processorSearcher;

    public Mono<Processing> create(String type, Map<String, String> arguments){
        processorSearcher.assertProcessorExists(type);

        final Processing processing = new Processing(type)
                .addArgs(arguments);
        return processingDAO.save(processing);
    }

    public Mono<Processing> findProcessing(String id){
        return processingDAO.findById(id)
                .switchIfEmpty(Mono.error(new EntityNotFoundException(id, Processing.class)));
    }

    public Flux<DataBuffer> findData(String id){
        return dataBufferStorage.findDataById(id)
                .switchIfEmpty(Mono.error(new EntityNotFoundException(id, Processing.class)));
    }

    public Mono<Processor.DownloadData> findDownloadData(String id) {
        return processingDAO.findById(id)
                .switchIfEmpty(Mono.error(new EntityNotFoundException(id, Processing.class)))
                .map(processing-> processorSearcher.findProcessor(processing.getType())
                        .orElseThrow()
                        .getDownloadData(processing));
    }

    /**
     * Return all stored processings with data in storage, but without data on database.
     * @return Flux with processings ids.
     */
    public Flux<String> findStoredProcessingWithoutDatabaseInfo() {
        return dataBufferStorage.findAllIds()
                .buffer(MAX_IDS_BATCH_EXISTS)
                .flatMap((ids)-> processingDAO.batchExists(ids));
    }

    /**
     * Delete processing from database and storage. Return a Tuple with success deletation about database and storage.
     * This information should be useful to calc batch deletion info.
     * @param id Processing Id
     * @return A Tuple2 with deletion result
     */
    public Mono<Tuple2<Boolean, Boolean>> deleteById(String id) {
        return processingDAO.deleteById(id)
                .zipWith(dataBufferStorage.deleteById(id));
    }

    public Flux<String> findProcessingsIdsCreatedBefore(LocalDateTime dateTime) {
        return processingDAO.findProcessingsIdsCreatedBefore(dateTime);
    }

    public Flux<Processing> findNewProcessings() {
        return processingDAO.findNewProcessings();
    }

    public Mono<LocalDateTime> saveStartProcessing(String id) {
        return processingDAO.saveStartProcessing(id);
    }

    public Mono<LocalDateTime> saveEndProcessing(String id) {
        return processingDAO.saveEndProcessing(id);
    }

    public Mono<Processing> saveErrorProcessing(String id, Throwable e) {
        return processingDAO.saveErrorProcessing(id, e);
    }
}
