package br.mvpcortes.batata.controller;

import br.mvpcortes.batata.service.EntityNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Map;

@ControllerAdvice(annotations = {BatataController.class})
public class BatataControllerAdvice {

    @ExceptionHandler
    public ResponseEntity<Map<String, String>> handle(IllegalArgumentException ex) {
        return ResponseEntity.badRequest().body(Map.of("message", ex.getMessage()));
    }

    @ExceptionHandler
    public ResponseEntity<Map<String, String>> handle(EntityNotFoundException entityNotFoundException){
        return
                ResponseEntity.notFound().build();
    }
}
