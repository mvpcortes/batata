package br.mvpcortes.batata.dao;

import br.mvpcortes.batata.model.Processing;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface ProcessingDAO {

    /**
     * Define way used by implemented DAO to generate unique ID for processing. By default
     * we going to use hashCode from type and arguments
     * @param type the type of processing
     * @param arguments the arguments of processing
     * @return the id in hashcode format
     */
    default String getId(String type, Map<String, String> arguments){
        return Processing.daoHashCode(type, arguments);
    }

    Mono<Processing> save(Processing p);

    Mono<Processing> findById(String id);

    Flux<Processing> findNewProcessings();

    Mono<Processing> saveErrorProcessing(String id, Throwable e);

    Mono<LocalDateTime> saveStartProcessing(String id);

    Mono<LocalDateTime> saveEndProcessing(String id);

    /**
     * Return processings created before 'minus' times just now;
     * @param minus the localDateTime for comparing before processing
     * @return The processings' ids.
     */
    Flux<String> findProcessingsIdsCreatedBefore(LocalDateTime minus);

    Mono<Boolean> deleteById(String id);

    Mono<Boolean> exists(String id);

    /**
     * Return only there are ids
     * @param ids list of ids to verify if exist.
     * @return the list of ids those exist.
     */
    Flux<String> batchExists(List<String> ids);
}
