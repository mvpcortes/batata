package br.mvpcortes.batata.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class ArgumentSerializer {

    private final ObjectMapper objectMapper;

    public ArgumentSerializer() {
        this(
                JsonMapper.builder().configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true).build()
        );
    }

    public ArgumentSerializer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String serialize(Map<String, String> arguments){
        try {
            return objectMapper.writeValueAsString(arguments);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Cannot write map to json: " + e.getMessage(), e);
        }
    }

    public Map<String, String> deserialize(String strArguments){
        return Optional.ofNullable(strArguments)
                .filter(str->!str.isBlank())
                .map(str-> {
                    try {
                        //noinspection unchecked
                        return (Map<String, String>)objectMapper.readValue(str, Map.class);
                    } catch (JsonProcessingException e) {
                        throw new IllegalStateException("Cannot read arguments from json: " + e.getMessage(), e);
                    }
                })
                .orElseGet(HashMap::new);
    }
}
