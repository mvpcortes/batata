package br.mvpcortes.batata.dao.jdbc;

import br.mvpcortes.batata.dao.ArgumentSerializer;
import br.mvpcortes.batata.dao.ProcessingDAO;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.util.ResultSetExtended;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public class ProcessingJdbcDAO implements ProcessingDAO {

    private JdbcTemplate jdbcTemplate;

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private BatataJdbcDAOProperties batataJdbcDAOProperties;

    private ArgumentSerializer argumentSerializer;

    public ProcessingJdbcDAO(JdbcTemplate jdbcTemplate, BatataJdbcDAOProperties batataJdbcDAOProperties, ArgumentSerializer argumentSerializer) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.batataJdbcDAOProperties = batataJdbcDAOProperties;
        this.argumentSerializer = argumentSerializer;
    }

    @Override
    public Mono<Processing> save(Processing p) {
        return Mono.create(ms->{
            p.setId(getId(p.getType(), p.getArguments()));
            final int qtd = jdbcTemplate.update("INSERT INTO "+ getDatabase()+".processing (id, type, create_time, arguments) VALUES (?, ?, ?, ?)",
                    p.getId(), p.getType(), p.getCreateTime(), argumentSerializer.serialize(p.getArguments()));

            if(qtd > 0){
                ms.success(p);
            }else{
                ms.error(new IllegalStateException("Invalid return '"+qtd+"' to update a INSERT processing"));
            }
        });
    }


    @Override
    public Mono<Processing> findById(String id) {
        return Mono.create(ms->{
            try {
                final Processing p = jdbcTemplate.queryForObject("SELECT * FROM "+ getDatabase()+".processing WHERE id = ?",
                        (rs, rowNum) -> toProcessing(rs), id);
                ms.success(p);
            }catch(EmptyResultDataAccessException er){
                ms.success();
            }catch(Exception e){
                ms.error(e);
            }
        });
    }

    @Override
    public Flux<Processing> findNewProcessings() {
        return Flux.create(ms-> {
            try {
                jdbcTemplate.query("SELECT * FROM "+getDatabase()+".processing WHERE start_time IS NULL ORDER BY create_time DESC", (rs) -> {
                    if(!ms.isCancelled()) {
                        ms.next(toProcessing(rs));
                    }
                });
            }catch(Exception e){
                ms.error(e);
            }finally {
                ms.complete();
            }
        });
    }

    @Override
    public Mono<Processing> saveErrorProcessing(String id, Throwable e) {
        return Mono.<String>create((ms)-> {
                    final LocalDateTime localDateTime = LocalDateTime.now();
                    jdbcTemplate.update("UPDATE "+getDatabase()+".processing SET error = ?, end_time = ? WHERE id = ?",
                            toString(e),
                            localDateTime,
                            id);
                    ms.success(id);
                })
                .flatMap(this::findById);
    }

    private String toString(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    @Override
    public Mono<LocalDateTime> saveStartProcessing(String id) {
        return Mono.create((ms)-> {
            final LocalDateTime localDateTime = LocalDateTime.now();
            jdbcTemplate.update("UPDATE "+getDatabase()+".processing SET start_time = ? WHERE id = ?", localDateTime, id);
            ms.success(localDateTime);
        });
    }

    @Override
    public Mono<LocalDateTime> saveEndProcessing(String id) {
        return Mono.create((ms)-> {
            final LocalDateTime localDateTime = LocalDateTime.now();
            jdbcTemplate.update("UPDATE "+getDatabase()+".processing SET end_time = ? WHERE id = ?", localDateTime, id);
            ms.success(localDateTime);
        });
    }

    @Override
    public Flux<String> findProcessingsIdsCreatedBefore(LocalDateTime value){
        return Flux.create(ms-> {
            try {
                jdbcTemplate.query("SELECT id FROM "+getDatabase()+".processing WHERE create_time <= ? ORDER BY create_time DESC", (rs) -> {
                            ms.next(rs.getString(1));
                        },
                        value
                );
            }catch(Exception e){
                ms.error(e);
            }finally {
                ms.complete();
            }
        });

    }

    @Override
    public Mono<Boolean> deleteById(String id){
        return Mono.create((ms)->{
            try{
                ms.success(jdbcTemplate.update("DELETE FROM "+getDatabase()+".processing WHERE id = ?", id) > 0);
            }catch(Exception e){
                ms.error(e);
            }
        });
    }

    @Override
    public Mono<Boolean> exists(String id) {
        return Mono.create(ms->{
            try {
                ms.success(jdbcTemplate.queryForObject("SELECT COUNT(*) FROM " + getDatabase() + ".processing WHERE id = ?",
                        (rs, rowNum) -> rs.getLong(1) > 0, id));
            }catch(Exception e){
                ms.error(e);
            }
        });
    }

    @Override
    public Flux<String> batchExists(final List<String> ids) {
        return Flux.create(ms -> {
            try {
                final SqlParameterSource parameters = new MapSqlParameterSource("ids", ids);
                namedParameterJdbcTemplate.query("SELECT id FROM " + getDatabase() + ".processing WHERE id IN (:ids)",
                        parameters, rs -> {ms.next(rs.getString(1));});
                ms.complete();
            } catch (Exception e) {
                ms.error(new IllegalStateException("Fail to query batchExists: " + e.getMessage(), e));
            }
        });
    }

    public void deleteAll(){
        jdbcTemplate.update("DELETE FROM " + getDatabase() + ".processing");
    }

    private Processing toProcessing(ResultSet resultSet) throws SQLException {
        return toProcessing(new ResultSetExtended(resultSet));
    }

    private Processing toProcessing(ResultSetExtended rse) throws SQLException {
        return new Processing()
                .setId(rse.getString("id"))
                .setType(rse.getString("type"))
                .setCreateTime(rse.getLocalDateTime("create_time"))
                .setStartTime(rse.getLocalDateTime("start_time"))
                .setEndTime(rse.getLocalDateTime("end_time"))
                .setArguments(argumentSerializer.deserialize(rse.getString("arguments")))
                .setError(rse.getString("error"));
    }


    private String getDatabase() {
        return batataJdbcDAOProperties.getDatabase();
    }

    public int count() {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM " + getDatabase() + ".processing", Integer.class);
    }
}
