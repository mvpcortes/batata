package br.mvpcortes.batata.dao.jdbc;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("batata.dao.jdbc")
@Getter
@Setter
public class BatataJdbcDAOProperties {
    private String database = "batata";
}
