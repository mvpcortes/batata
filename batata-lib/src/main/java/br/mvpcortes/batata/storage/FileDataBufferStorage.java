package br.mvpcortes.batata.storage;

import br.mvpcortes.batata.service.BatataFileStorageProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;

@Repository
@Slf4j
@Profile("!memory-storage")
public class FileDataBufferStorage implements DataBufferStorage{

    private final int BUFFER_SIZE = 1024;

    private final BatataFileStorageProperties batataFileStorageProperties;

    private final DataBufferFactory defaultDataBufferFactory = new DefaultDataBufferFactory();

    public FileDataBufferStorage(BatataFileStorageProperties batataFileStorageProperties) {
        this.batataFileStorageProperties = batataFileStorageProperties;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @PostConstruct
    public void init(){
        if(batataFileStorageProperties.getPath() == null){
            log.warn("batata.storage.file.path not defined. Create path in current place.");
            batataFileStorageProperties.setPath(Paths.get("").toAbsolutePath());
            if(!batataFileStorageProperties.getPath().toFile().exists()){
                batataFileStorageProperties.getPath().toFile().mkdir();
            }
        }
    }

    @Override
    public Mono<String> save(String id, Flux<DataBuffer> fluxDataBuffer) {
        return DataBufferUtils
                .write(fluxDataBuffer, getPathById(id),
                        StandardOpenOption.CREATE , StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING)
//                    .doOnRequest(i->log.warn("request: {}", i))
//                    .doOnEach(x->log.warn("each: {}", x))
//                    .doOnCancel(()->log.warn("Achei um cancel!"))
//                    .doOnError(x->log.warn("Achei um erro! {}", x.getMessage(), x))
                .then(Mono.just(id));
//                    .map(v->id);
    }

    @Override
    public Flux<DataBuffer> findDataById(String id) {
        return Mono.just(getPathById(id))
                .flux()
                .flatMap(path-> DataBufferUtils.read(path, defaultDataBufferFactory, BUFFER_SIZE))
                .doOnNext(DataBufferUtils::release);
    }

    @Override
    public Mono<Boolean> deleteById(String id){
        return Mono.create(ms->{
            try{
                Files.delete(getPathById(id));
                ms.success(true);
            }catch(NoSuchFileException nfe){
                ms.success(false);
            }catch(Exception e){
                ms.error(e);
            }
        });
    }

    @Override
    public Flux<String> findAllIds() {

        return Flux.create(ms-> {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(batataFileStorageProperties.getPath())) {
                for (Path path : stream) {
                    if (!Files.isDirectory(path)) {
                        ms.next(path.getFileName().toString());
                    }
                }
                ms.complete();
            } catch (IOException e) {
                ms.error(e);
            }
        });
    }


    public Path getPathById(String id){
        return batataFileStorageProperties.getPath().resolve(id);
    }
}
