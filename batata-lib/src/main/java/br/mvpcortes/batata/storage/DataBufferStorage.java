package br.mvpcortes.batata.storage;

import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DataBufferStorage {

    Mono<String> save(String id, Flux<DataBuffer> fluxDataBuffer);

    Flux<DataBuffer> findDataById(String id);

    Mono<Boolean> deleteById(String id);

    Flux<String> findAllIds();
}
