package br.mvpcortes.batata.storage;

import br.mvpcortes.batata.util.ByteArrayUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@Profile("memory-storage")
public class MemoryDataBufferStorage implements DataBufferStorage {

    private final DataBufferFactory defaultDataBufferFactory = new DefaultDataBufferFactory();

    private final Map<String, byte[]> map = new ConcurrentHashMap<>();

    @Override
    public Mono<String> save(String id, Flux<DataBuffer> fluxDataBuffer) {
        return fluxDataBuffer
                .map(dataBuffer-> {
                    final byte[] bytes = new byte[dataBuffer.readableByteCount()];
                    dataBuffer.read(bytes);
                    return bytes;
                })
                .collect(toList())
                .map(ByteArrayUtils::merge)
                .map(bytes->{
                    map.put(id, bytes);
                    return bytes;
                })
                .then(Mono.just(id))
                .doOnError(e->log.error("a",e));
    }

    @Override
    public Flux<DataBuffer> findDataById(String id) {
        return Optional.ofNullable(map.get(id))
                .map(bytes->Flux.just(defaultDataBufferFactory.allocateBuffer().write(bytes)))
                .orElse(Flux.empty());
    }

    @Override
    public Mono<Boolean> deleteById(String id) {
        return  Mono.fromSupplier(()-> map.remove(id) != null);
    }

    @Override
    public Flux<String> findAllIds() {
        return Flux.fromStream(map.keySet().stream());
    }

    public byte[] findRawData(String s) {
        return map.get(s);
    }
}
