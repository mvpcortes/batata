package br.mvpcortes.batata.config;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
@ConfigurationProperties("batata.scheduler")
@Getter
@Setter
@Accessors(chain = true)
public class BatataSchedulerProperties {

   private boolean enabled = true;
   private String type;

   private Duration cleanAfter;
}
