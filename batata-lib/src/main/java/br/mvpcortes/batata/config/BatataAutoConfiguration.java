package br.mvpcortes.batata.config;

import br.mvpcortes.batata.dao.ArgumentSerializer;
import br.mvpcortes.batata.dao.Batata;
import br.mvpcortes.batata.dao.ProcessingDAO;
import br.mvpcortes.batata.dao.jdbc.BatataJdbcDAOProperties;
import br.mvpcortes.batata.dao.jdbc.ProcessingJdbcDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.Optional;

@Configuration
@Slf4j
@EnableScheduling
public class BatataAutoConfiguration {

    @Bean
    public Scheduler batataScheduler(BatataSchedulerProperties batataSchedulerProperties){
        final String sc = Optional.ofNullable(batataSchedulerProperties.getType()).map(String::toUpperCase).orElse("");
        switch (sc){
            case "IMMEDIATE":
                return Schedulers.immediate();
            case "BOUNDED_ELASTIC":
                return Schedulers.boundedElastic();
            default:
                log.warn("Cannot found scheduler '{}'. Using IMMEDIATE.", sc);
                return Schedulers.immediate();
        }
    }

    @Bean
    @ConditionalOnBean(value = JdbcTemplate.class, annotation = Batata.class)
    public ProcessingDAO processingDAOQualifier(@Batata JdbcTemplate jdbcTemplate,
                                                BatataJdbcDAOProperties batataJdbcDAOProperties,
                                                ArgumentSerializer argumentSerializer){
        return new ProcessingJdbcDAO(jdbcTemplate, batataJdbcDAOProperties, argumentSerializer);
    }
    @Bean
    @ConditionalOnMissingBean
    public ProcessingDAO processingDAO(JdbcTemplate jdbcTemplate,
                                       BatataJdbcDAOProperties batataJdbcDAOProperties,
                                       ArgumentSerializer argumentSerializer){
        return new ProcessingJdbcDAO(jdbcTemplate, batataJdbcDAOProperties, argumentSerializer);
    }

}
