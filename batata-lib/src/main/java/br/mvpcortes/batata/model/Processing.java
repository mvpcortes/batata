package br.mvpcortes.batata.model;

import com.google.common.hash.Hashing;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Processing {

    public enum Status{
        CREATED, PROCESSING, ENDED, FAILED
    }

    public static String daoHashCode(String type, Map<String, String> arguments) {
        final String hh = Hashing.sha256()
                .hashBytes(toByteArray(type, arguments))
                .toString();
        log.debug("daoHashCode: {}, {}, {}", type, arguments, hh);
        return hh;
    }

    private static byte[] toByteArray(String type, Map<String, String> arguments) {
        return Stream.concat(
                        Stream.of(type),
                        arguments.entrySet().stream().map(entry -> entry.getKey() + "-" + entry.getValue())).sorted()
                .collect(Collectors.joining())
                .getBytes(UTF_8);
    }

    private String id;
    private String type;
    private LocalDateTime createTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private Map<String, String> arguments = new HashMap<>();

    private String error = null;

    public Processing(String type){
        this.type = type;
        this.createTime = LocalDateTime.now();
        this.arguments = new HashMap<>();
    }
    public Processing(String type, Map<String, String> arguments){
        this(type);
        this.arguments.putAll(arguments);
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", id, getStatus());
    }

    public Processing addArg(String key, String value){
        arguments.put(key, value);
        return this;
    }

    public Processing addArgs(Map<String, String> args){
        arguments.putAll(args);
        return this;
    }

    public String getArgument(String key) {
        return arguments.get(key);
    }

    public Integer getIntArgument(@NonNull String key){
        return Optional.ofNullable(arguments.get(key))
                .map(Integer::parseInt)
                .orElse(null);
    }

    public Long getLongArgument(String key){
        return Optional.ofNullable(arguments.get(key))
                .map(Long::parseLong)
                .orElse(null);
    }

    public Boolean getBooleanArgument(String key){
        return Optional.ofNullable(arguments.get(key))
                .map(Boolean::parseBoolean)
                .orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Processing that = (Processing) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(arguments, that.arguments);
    }

    public int hashCode(){
        return daoHashCode().hashCode();
    }

    public String daoHashCode(){
        return  daoHashCode(type, arguments);
    }

    public Status getStatus() {
        if(this.error != null){
            return Status.FAILED;
        }else if(this.endTime != null){
            return  Status.ENDED;
        }else if(this.startTime != null){
            return Status.PROCESSING;
        }else {
            return Status.CREATED;
        }
    }
}
