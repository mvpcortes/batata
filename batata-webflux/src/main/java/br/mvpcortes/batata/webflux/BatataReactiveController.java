package br.mvpcortes.batata.webflux;

import br.mvpcortes.batata.controller.BatataController;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.ProcessingService;
import br.mvpcortes.batata.util.CreateValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@BatataController
@RequestMapping("${batata.controller.path:'batata'}")
public class BatataReactiveController {

    @Autowired
    private ProcessingService processingService;

    @PostMapping
    public Mono<Processing> create(@RequestBody CreateValue createValue){
        return processingService.create(createValue.getType(), createValue.getArguments());
    }

    @GetMapping("{id}")
    public Mono<Processing> findProcessing(@PathVariable String id){
        return processingService.findProcessing(id);
    }

    @GetMapping("{id}/data")
    public Mono<ResponseEntity<Flux<DataBuffer>>> findData(@PathVariable String id){
        return
                processingService.findDownloadData(id)
                .map(downloadData-> ResponseEntity.ok()
                        .header("Content-Disposition", String.format("attachment; filename=%s", downloadData.fileName()))
                        .header("Set-Cookie", "fileDownload=true; path=/")
                        .contentType(downloadData.mediaType())
                        .body(processingService.findData(id)));
    }
}
