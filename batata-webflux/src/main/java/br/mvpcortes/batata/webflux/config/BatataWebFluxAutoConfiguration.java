package br.mvpcortes.batata.webflux.config;

import br.mvpcortes.batata.controller.BatataControllerAdvice;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(BatataControllerAdvice.class)
public class BatataWebFluxAutoConfiguration {

}
