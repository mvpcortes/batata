package br.mvpcortes.batata.webflux;

import br.mvpcortes.batata.dao.jdbc.ProcessingJdbcDAO;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.ProcessingSchedule;
import br.mvpcortes.batata.service.ProcessingService;
import br.mvpcortes.batata.util.CreateValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {TestWebFluxApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("memory-storage")
public class BatataReactiveControllerTest {

    @Autowired
    WebTestClient webTestClient;

    @Autowired
    private ProcessingService processingService;

    @Autowired
    private ProcessingSchedule processingSchedule;

    @Autowired
    private ProcessingJdbcDAO processingJdbcDAO;

    @BeforeEach
    public void init(){
        processingJdbcDAO.deleteAll();
    }

    @Test
    public void when_try_create_processing_with_type_not_founded_then_fail(){
        final CreateValue createValue = new CreateValue("i_do_not_exists",
                Map.of("a" , "1", "b", "2", "c", "3")
        );
        webTestClient.mutate().responseTimeout(Duration.ofMinutes(30))
                .build()
                .post()
                .uri("batata")
                .body(Mono.just(createValue), CreateValue.class)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.BAD_REQUEST)
                .expectBody(Map.class)
                .value(map->{
                    assertThat(map).containsOnlyKeys("message");
                    assertThat(map).containsEntry("message", "Processor for type 'i_do_not_exists' not exists");

                });
    }

    @Test
    public void when_try_create_processing_then_save_it_and_return_processing_saved(){
        final CreateValue createValue = new CreateValue("json_argument",
                Map.of("a" , "1", "b", "2", "c", "3")
        );
        final LocalDateTime before = LocalDateTime.now();
        webTestClient.mutate().responseTimeout(Duration.ofMinutes(30))
                .build()
                .post()
                .uri("batata")
                .body(Mono.just(createValue), CreateValue.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Processing.class)
                .value(processing -> {
                    assertThat(processing.getStatus()).isEqualTo(Processing.Status.CREATED);
                    assertThat(processing.getArguments()).isEqualTo(createValue.getArguments());
                    assertThat(processing.getType()).isEqualTo(createValue.getType());
                    assertThat(processing.getCreateTime()).isAfter(before);
                    assertThat(processing.getStartTime()).isNull();
                    assertThat(processing.getEndTime()).isNull();
                    assertThat(processing.getError()).isNull();
                });
    }

    @Test
    public void when_saved_processing_then_its_data_is_returned_by_request_using_id(){
        final LocalDateTime before = LocalDateTime.now();

        final String id = processingService.create("json_argument", Map.of("a", "1", "b", "2")).block().getId();

        webTestClient.mutate().responseTimeout(Duration.ofMinutes(30))
                .build()
                .get()
                .uri("batata/{id}", Map.of("id", id))
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Processing.class)
                .value(processing -> {
                    assertThat(processing.getStatus()).isEqualTo(Processing.Status.CREATED);
                    assertThat(processing.getArguments()).isEqualTo(Map.of("a", "1", "b", "2"));
                    assertThat(processing.getType()).isEqualTo("json_argument");
                    assertThat(processing.getCreateTime()).isAfter(before);
                    assertThat(processing.getStartTime()).isNull();
                    assertThat(processing.getEndTime()).isNull();
                    assertThat(processing.getError()).isNull();
                });
    }

    @Test
    public void when_try_get_processing_and_not_exists_processing_then_return_404(){
        webTestClient.mutate().responseTimeout(Duration.ofMinutes(30))
                .build()
                .get()
                .uri("batata/{id}", Map.of("id", "xuxuxuxuxuxux"))
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void when_saved_processing_processed_then_can_get_data(){
        final String id = processingService.create("json_argument", Map.of("a", "1", "b", "2")).block().getId();

        processingSchedule.runProcessings();

        webTestClient.mutate().responseTimeout(Duration.ofMinutes(30))
                .build()
                .get()
                .uri("batata/{id}/data", Map.of("id", id))
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectHeader().valueEquals("Content-Disposition", String.format("attachment; filename=%s.json", id))
                .expectBody(String.class)
                .value(str-> assertThat(str).isEqualTo("{\"a\":\"1\",\"b\":\"2\"}"));
    }

    @Test
    public void when_try_get_data_and_not_exists_processing_then_return_404(){
        webTestClient.mutate().responseTimeout(Duration.ofMinutes(30))
                .build()
                .get()
                .uri("batata/{id}/data", Map.of("id", "xuxuxuxuxuxux"))
                .exchange()
                .expectStatus().isNotFound();
    }

}