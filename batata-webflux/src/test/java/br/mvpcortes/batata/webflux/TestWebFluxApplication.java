package br.mvpcortes.batata.webflux;

import br.mvpcortes.batata.service.processor.JsonArgumentProcessor;
import br.mvpcortes.batata.service.processor.Processor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = {"br.mvpcortes.batata"})
public class TestWebFluxApplication {
    @TestConfiguration
    public static class ScheculeConfiguration{
        @Bean
        public Processor jsonArgumentProcessor(){
            return new JsonArgumentProcessor();
        }
    }
}

