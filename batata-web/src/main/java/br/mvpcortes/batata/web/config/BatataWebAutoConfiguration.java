package br.mvpcortes.batata.web.config;

import br.mvpcortes.batata.controller.BatataControllerAdvice;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(BatataControllerAdvice.class)
@ComponentScan(basePackages = "br.mvpcortes.batata")
public class BatataWebAutoConfiguration {

}
