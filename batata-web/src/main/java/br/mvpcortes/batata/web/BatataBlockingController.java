package br.mvpcortes.batata.web;

import br.mvpcortes.batata.controller.BatataController;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.ProcessingBlockingService;
import br.mvpcortes.batata.service.processor.Processor;
import br.mvpcortes.batata.util.CreateValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@BatataController
@RequestMapping("${batata.controller.path:batata}")
public class BatataBlockingController {

    @Autowired
    private ProcessingBlockingService processingService;

    @PostMapping
    public Processing create(@RequestBody CreateValue createValue){
        return processingService.create(createValue.getType(), createValue.getArguments());
    }

    @GetMapping("{id}")
    public Processing findProcessing(@PathVariable String id){
        return processingService.findProcessing(id);
    }

    @GetMapping("{id}/data")
    public  ResponseEntity<StreamingResponseBody> findData(
            @PathVariable String id) {
        final Processor.DownloadData downloadData = processingService.findDownloadData(id);

        return ResponseEntity.ok()
                .header("Content-Disposition", String.format("attachment; filename=%s", downloadData.fileName()))
                .header("Set-Cookie", "fileDownload=true; path=/")
                .contentType(downloadData.mediaType())
                .body(outputStream -> processingService.findData(id, outputStream));
    }
}
