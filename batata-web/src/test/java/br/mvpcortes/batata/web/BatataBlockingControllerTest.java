package br.mvpcortes.batata.web;


import br.mvpcortes.batata.dao.jdbc.ProcessingJdbcDAO;
import br.mvpcortes.batata.model.Processing;
import br.mvpcortes.batata.service.ProcessingBlockingService;
import br.mvpcortes.batata.service.ProcessingSchedule;
import br.mvpcortes.batata.util.CreateValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {TestWebApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("memory-storage")
public class BatataBlockingControllerTest {


    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate testRestTemplate;


    @Autowired
    private ProcessingBlockingService processingService;

    @Autowired
    private ProcessingSchedule processingSchedule;

    @Autowired
    private ProcessingJdbcDAO processingJdbcDAO;

    @BeforeEach
    public void init(){
        processingJdbcDAO.deleteAll();
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void when_try_create_processing_with_type_not_founded_then_fail(){
        final CreateValue createValue = new CreateValue("i_do_not_exists",
                Map.of("a" , "1", "b", "2", "c", "3")
        );

        ResponseEntity<Map> responseEntity = testRestTemplate.postForEntity("http://localhost:" + port + "/batata",
                createValue , Map.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isEqualTo(Map.of("message", "Processor for type 'i_do_not_exists' not exists"));
    }

    @Test
    public void when_try_create_processing_then_save_it_and_return_processing_saved(){
        final CreateValue createValue = new CreateValue("json_argument",
                Map.of("a" , "1", "b", "2", "c", "3")
        );

        LocalDateTime before = LocalDateTime.now();

        ResponseEntity<Processing> responseEntity = testRestTemplate.postForEntity("http://localhost:" + port + "/batata",
                createValue , Processing.class);

        Processing processing = responseEntity.getBody();
        assertCreatedProcessing(createValue, before, processing);
    }

    @Test
    public void when_saved_processing_then_its_data_is_returned_by_request_using_id(){
        final LocalDateTime before = LocalDateTime.now();

        final CreateValue createValue = new CreateValue("json_argument",
                Map.of("a", "1", "b", "2")
        );

        final String id = processingService.create(createValue.getType(), createValue.getArguments()).getId();

        ResponseEntity<Processing> responseEntity = testRestTemplate
                .getForEntity("http://localhost:" + port + "/batata/{id}",
                Processing.class,
                Map.of("id", id));

        assertCreatedProcessing(createValue, before, responseEntity.getBody());
    }


    private void assertCreatedProcessing(CreateValue createValue, LocalDateTime before, Processing processing) {
        assertThat(processing.getStatus()).isEqualTo(Processing.Status.CREATED);
        assertThat(processing.getArguments()).isEqualTo(createValue.getArguments());
        assertThat(processing.getType()).isEqualTo(createValue.getType());
        assertThat(processing.getCreateTime()).isAfter(before);
        assertThat(processing.getStartTime()).isNull();
        assertThat(processing.getEndTime()).isNull();
        assertThat(processing.getError()).isNull();
    }

    @Test
    public void when_try_get_processing_and_not_exists_processing_then_return_404(){

        ResponseEntity<Processing> responseEntity = testRestTemplate
                .getForEntity("http://localhost:" + port + "/batata/{id}",
                        Processing.class,
                        Map.of("id", "238888908293084293084902184901890418904829048190482904ufoiuewoiwu"));

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void when_saved_processing_processed_then_can_get_data(){
        final String id = processingService.create("json_argument", Map.of("a", "1", "b", "2")).getId();

        processingSchedule.runProcessings();


        ResponseEntity<String> responseEntity = testRestTemplate
                .getForEntity("http://localhost:" + port + "/batata/{id}/data",
                        String.class,
                        Map.of("id", id));

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getHeaders().get("Content-Type")).isEqualTo(List.of(MediaType.APPLICATION_JSON.toString()));
        assertThat(responseEntity.getHeaders().get("Content-Disposition")).isEqualTo(List.of(String.format("attachment; filename=%s.json", id)));

        assertThat(responseEntity.getBody()).isEqualTo("{\"a\":\"1\",\"b\":\"2\"}");
    }

    @Test
    public void when_try_get_data_and_not_exists_processing_then_return_404(){
        ResponseEntity<String> responseEntity = testRestTemplate
                .getForEntity("http://localhost:" + port + "/batata/{id}/data",
                        String.class,
                        Map.of("id", "8237897897898989347837373289723897489"));

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

}