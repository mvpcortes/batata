package br.mvpcortes.batata.web;

import br.mvpcortes.batata.service.processor.JsonArgumentProcessor;
import br.mvpcortes.batata.service.processor.Processor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TestWebApplication {
    @TestConfiguration
    public static class ScheculeConfiguration{
        @Bean
        public Processor jsonArgumentProcessor(){
            return new JsonArgumentProcessor();
        }
    }
}

